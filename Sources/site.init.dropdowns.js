﻿/* Part of nuget package */
site.addHtmlInitializer(function () {
    var dropdown = new Dropdown(self);

    var applySelector = function (select, dataQuery, initSelection) {
        var allowClear = select.hasClass("select-clear");

        var isReadOnly = select.is("[disabled]") || select.is("[readonly]");
        var placeholder = "Please choose value";
        if (isReadOnly) {
            placeholder = "";
        }

        select.select2({
            dropdownAutoWidth: true,
            noFocus: !select.data("focus"),
            allowClear: allowClear,
            placeholder: placeholder,
            minimumResultsForSearch: -1,
            query: dataQuery,
            initSelection: initSelection
        });
        if (isReadOnly) {
            select.select2("readonly", true);
        }

        select.on("select2-selecting", function () {
            setTimeout(function () {
                if (select.parents("form").length) {
                    select.valid();
                    $(this).focus();
                }
            }, 0);
        });
    };

    $(".ajax-ddl-select", this).each(function () {
        var select = $(this);
        if (!select.hasClass("self-init")) {
            dropdown.apply(select);
        }
    });

    $("select", this).each(function () {
        applySelector($(this));
    });


    $(".ddl-with-children", this).each(function () {
        var select = $(this);
        var data = select.data("items");
        
        select.select2({
            multiple: true,
            data: data,
            initSelection: function (element, callback) {
                var selectedItems = select.val().split(",");
                var selected = [];
                $.map(data, function (group) {
                    $.each(group.children, function (i, child) {
                        if (selectedItems.indexOf(child.id + "") > -1) {
                            selected.push(child);
                        }
                    });
                });
                callback(selected);
            },
            query: function (options) {
                var selectedIds = this.element.select2('val');
                var selectableGroups = $.map(this.data, function (group) {
                    var areChildrenAllSelected = true;
                    $.each(group.children, function (i, child) {
                        if (selectedIds.indexOf(child.id + "") < 0) {
                            areChildrenAllSelected = false;
                            return false;
                        }
                    });
                    return !areChildrenAllSelected ? group : null;
                });
                options.callback({ results: selectableGroups });
            }
        }).on('select2-selecting', function (e) {
            var $select = $(this);
            if (!e.val) {
                e.preventDefault();

                $.each(e.object.children, function (i, child) {
                    if (child.text.indexOf(e.object.text) < 0)
                        child.text = e.object.text + " - " + child.text;
                });

                $select.select2('data', $select.select2('data').concat(e.object.children));
                $select.select2('close');
                $select.trigger("select2-selected");
            } else {
                $.map(data, function (group) {
                    $.each(group.children, function (i, child) {
                        if (child.id === e.val) {
                            e.object.text = group.text + " - " + e.object.text;
                        }
                    });
                });
            }
        }).on('select2-removed', function (e) {
            var splitted = e.choice.text.split(" - ");
            if (splitted.length >= 1) {
                e.choice.text = splitted[1];
            }
        });
    });
});