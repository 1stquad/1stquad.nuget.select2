﻿/* Part of nuget package */
function Dropdown(site) {
    "use strict";

    //#region Markup

    var diacritics = { "\u24B6": "A", "\uFF21": "A", "\u00C0": "A", "\u00C1": "A", "\u00C2": "A", "\u1EA6": "A", "\u1EA4": "A", "\u1EAA": "A", "\u1EA8": "A", "\u00C3": "A", "\u0100": "A", "\u0102": "A", "\u1EB0": "A", "\u1EAE": "A", "\u1EB4": "A", "\u1EB2": "A", "\u0226": "A", "\u01E0": "A", "\u00C4": "A", "\u01DE": "A", "\u1EA2": "A", "\u00C5": "A", "\u01FA": "A", "\u01CD": "A", "\u0200": "A", "\u0202": "A", "\u1EA0": "A", "\u1EAC": "A", "\u1EB6": "A", "\u1E00": "A", "\u0104": "A", "\u023A": "A", "\u2C6F": "A", "\uA732": "AA", "\u00C6": "AE", "\u01FC": "AE", "\u01E2": "AE", "\uA734": "AO", "\uA736": "AU", "\uA738": "AV", "\uA73A": "AV", "\uA73C": "AY", "\u24B7": "B", "\uFF22": "B", "\u1E02": "B", "\u1E04": "B", "\u1E06": "B", "\u0243": "B", "\u0182": "B", "\u0181": "B", "\u24B8": "C", "\uFF23": "C", "\u0106": "C", "\u0108": "C", "\u010A": "C", "\u010C": "C", "\u00C7": "C", "\u1E08": "C", "\u0187": "C", "\u023B": "C", "\uA73E": "C", "\u24B9": "D", "\uFF24": "D", "\u1E0A": "D", "\u010E": "D", "\u1E0C": "D", "\u1E10": "D", "\u1E12": "D", "\u1E0E": "D", "\u0110": "D", "\u018B": "D", "\u018A": "D", "\u0189": "D", "\uA779": "D", "\u01F1": "DZ", "\u01C4": "DZ", "\u01F2": "Dz", "\u01C5": "Dz", "\u24BA": "E", "\uFF25": "E", "\u00C8": "E", "\u00C9": "E", "\u00CA": "E", "\u1EC0": "E", "\u1EBE": "E", "\u1EC4": "E", "\u1EC2": "E", "\u1EBC": "E", "\u0112": "E", "\u1E14": "E", "\u1E16": "E", "\u0114": "E", "\u0116": "E", "\u00CB": "E", "\u1EBA": "E", "\u011A": "E", "\u0204": "E", "\u0206": "E", "\u1EB8": "E", "\u1EC6": "E", "\u0228": "E", "\u1E1C": "E", "\u0118": "E", "\u1E18": "E", "\u1E1A": "E", "\u0190": "E", "\u018E": "E", "\u24BB": "F", "\uFF26": "F", "\u1E1E": "F", "\u0191": "F", "\uA77B": "F", "\u24BC": "G", "\uFF27": "G", "\u01F4": "G", "\u011C": "G", "\u1E20": "G", "\u011E": "G", "\u0120": "G", "\u01E6": "G", "\u0122": "G", "\u01E4": "G", "\u0193": "G", "\uA7A0": "G", "\uA77D": "G", "\uA77E": "G", "\u24BD": "H", "\uFF28": "H", "\u0124": "H", "\u1E22": "H", "\u1E26": "H", "\u021E": "H", "\u1E24": "H", "\u1E28": "H", "\u1E2A": "H", "\u0126": "H", "\u2C67": "H", "\u2C75": "H", "\uA78D": "H", "\u24BE": "I", "\uFF29": "I", "\u00CC": "I", "\u00CD": "I", "\u00CE": "I", "\u0128": "I", "\u012A": "I", "\u012C": "I", "\u0130": "I", "\u00CF": "I", "\u1E2E": "I", "\u1EC8": "I", "\u01CF": "I", "\u0208": "I", "\u020A": "I", "\u1ECA": "I", "\u012E": "I", "\u1E2C": "I", "\u0197": "I", "\u24BF": "J", "\uFF2A": "J", "\u0134": "J", "\u0248": "J", "\u24C0": "K", "\uFF2B": "K", "\u1E30": "K", "\u01E8": "K", "\u1E32": "K", "\u0136": "K", "\u1E34": "K", "\u0198": "K", "\u2C69": "K", "\uA740": "K", "\uA742": "K", "\uA744": "K", "\uA7A2": "K", "\u24C1": "L", "\uFF2C": "L", "\u013F": "L", "\u0139": "L", "\u013D": "L", "\u1E36": "L", "\u1E38": "L", "\u013B": "L", "\u1E3C": "L", "\u1E3A": "L", "\u0141": "L", "\u023D": "L", "\u2C62": "L", "\u2C60": "L", "\uA748": "L", "\uA746": "L", "\uA780": "L", "\u01C7": "LJ", "\u01C8": "Lj", "\u24C2": "M", "\uFF2D": "M", "\u1E3E": "M", "\u1E40": "M", "\u1E42": "M", "\u2C6E": "M", "\u019C": "M", "\u24C3": "N", "\uFF2E": "N", "\u01F8": "N", "\u0143": "N", "\u00D1": "N", "\u1E44": "N", "\u0147": "N", "\u1E46": "N", "\u0145": "N", "\u1E4A": "N", "\u1E48": "N", "\u0220": "N", "\u019D": "N", "\uA790": "N", "\uA7A4": "N", "\u01CA": "NJ", "\u01CB": "Nj", "\u24C4": "O", "\uFF2F": "O", "\u00D2": "O", "\u00D3": "O", "\u00D4": "O", "\u1ED2": "O", "\u1ED0": "O", "\u1ED6": "O", "\u1ED4": "O", "\u00D5": "O", "\u1E4C": "O", "\u022C": "O", "\u1E4E": "O", "\u014C": "O", "\u1E50": "O", "\u1E52": "O", "\u014E": "O", "\u022E": "O", "\u0230": "O", "\u00D6": "O", "\u022A": "O", "\u1ECE": "O", "\u0150": "O", "\u01D1": "O", "\u020C": "O", "\u020E": "O", "\u01A0": "O", "\u1EDC": "O", "\u1EDA": "O", "\u1EE0": "O", "\u1EDE": "O", "\u1EE2": "O", "\u1ECC": "O", "\u1ED8": "O", "\u01EA": "O", "\u01EC": "O", "\u00D8": "O", "\u01FE": "O", "\u0186": "O", "\u019F": "O", "\uA74A": "O", "\uA74C": "O", "\u01A2": "OI", "\uA74E": "OO", "\u0222": "OU", "\u24C5": "P", "\uFF30": "P", "\u1E54": "P", "\u1E56": "P", "\u01A4": "P", "\u2C63": "P", "\uA750": "P", "\uA752": "P", "\uA754": "P", "\u24C6": "Q", "\uFF31": "Q", "\uA756": "Q", "\uA758": "Q", "\u024A": "Q", "\u24C7": "R", "\uFF32": "R", "\u0154": "R", "\u1E58": "R", "\u0158": "R", "\u0210": "R", "\u0212": "R", "\u1E5A": "R", "\u1E5C": "R", "\u0156": "R", "\u1E5E": "R", "\u024C": "R", "\u2C64": "R", "\uA75A": "R", "\uA7A6": "R", "\uA782": "R", "\u24C8": "S", "\uFF33": "S", "\u1E9E": "S", "\u015A": "S", "\u1E64": "S", "\u015C": "S", "\u1E60": "S", "\u0160": "S", "\u1E66": "S", "\u1E62": "S", "\u1E68": "S", "\u0218": "S", "\u015E": "S", "\u2C7E": "S", "\uA7A8": "S", "\uA784": "S", "\u24C9": "T", "\uFF34": "T", "\u1E6A": "T", "\u0164": "T", "\u1E6C": "T", "\u021A": "T", "\u0162": "T", "\u1E70": "T", "\u1E6E": "T", "\u0166": "T", "\u01AC": "T", "\u01AE": "T", "\u023E": "T", "\uA786": "T", "\uA728": "TZ", "\u24CA": "U", "\uFF35": "U", "\u00D9": "U", "\u00DA": "U", "\u00DB": "U", "\u0168": "U", "\u1E78": "U", "\u016A": "U", "\u1E7A": "U", "\u016C": "U", "\u00DC": "U", "\u01DB": "U", "\u01D7": "U", "\u01D5": "U", "\u01D9": "U", "\u1EE6": "U", "\u016E": "U", "\u0170": "U", "\u01D3": "U", "\u0214": "U", "\u0216": "U", "\u01AF": "U", "\u1EEA": "U", "\u1EE8": "U", "\u1EEE": "U", "\u1EEC": "U", "\u1EF0": "U", "\u1EE4": "U", "\u1E72": "U", "\u0172": "U", "\u1E76": "U", "\u1E74": "U", "\u0244": "U", "\u24CB": "V", "\uFF36": "V", "\u1E7C": "V", "\u1E7E": "V", "\u01B2": "V", "\uA75E": "V", "\u0245": "V", "\uA760": "VY", "\u24CC": "W", "\uFF37": "W", "\u1E80": "W", "\u1E82": "W", "\u0174": "W", "\u1E86": "W", "\u1E84": "W", "\u1E88": "W", "\u2C72": "W", "\u24CD": "X", "\uFF38": "X", "\u1E8A": "X", "\u1E8C": "X", "\u24CE": "Y", "\uFF39": "Y", "\u1EF2": "Y", "\u00DD": "Y", "\u0176": "Y", "\u1EF8": "Y", "\u0232": "Y", "\u1E8E": "Y", "\u0178": "Y", "\u1EF6": "Y", "\u1EF4": "Y", "\u01B3": "Y", "\u024E": "Y", "\u1EFE": "Y", "\u24CF": "Z", "\uFF3A": "Z", "\u0179": "Z", "\u1E90": "Z", "\u017B": "Z", "\u017D": "Z", "\u1E92": "Z", "\u1E94": "Z", "\u01B5": "Z", "\u0224": "Z", "\u2C7F": "Z", "\u2C6B": "Z", "\uA762": "Z", "\u24D0": "a", "\uFF41": "a", "\u1E9A": "a", "\u00E0": "a", "\u00E1": "a", "\u00E2": "a", "\u1EA7": "a", "\u1EA5": "a", "\u1EAB": "a", "\u1EA9": "a", "\u00E3": "a", "\u0101": "a", "\u0103": "a", "\u1EB1": "a", "\u1EAF": "a", "\u1EB5": "a", "\u1EB3": "a", "\u0227": "a", "\u01E1": "a", "\u00E4": "a", "\u01DF": "a", "\u1EA3": "a", "\u00E5": "a", "\u01FB": "a", "\u01CE": "a", "\u0201": "a", "\u0203": "a", "\u1EA1": "a", "\u1EAD": "a", "\u1EB7": "a", "\u1E01": "a", "\u0105": "a", "\u2C65": "a", "\u0250": "a", "\uA733": "aa", "\u00E6": "ae", "\u01FD": "ae", "\u01E3": "ae", "\uA735": "ao", "\uA737": "au", "\uA739": "av", "\uA73B": "av", "\uA73D": "ay", "\u24D1": "b", "\uFF42": "b", "\u1E03": "b", "\u1E05": "b", "\u1E07": "b", "\u0180": "b", "\u0183": "b", "\u0253": "b", "\u24D2": "c", "\uFF43": "c", "\u0107": "c", "\u0109": "c", "\u010B": "c", "\u010D": "c", "\u00E7": "c", "\u1E09": "c", "\u0188": "c", "\u023C": "c", "\uA73F": "c", "\u2184": "c", "\u24D3": "d", "\uFF44": "d", "\u1E0B": "d", "\u010F": "d", "\u1E0D": "d", "\u1E11": "d", "\u1E13": "d", "\u1E0F": "d", "\u0111": "d", "\u018C": "d", "\u0256": "d", "\u0257": "d", "\uA77A": "d", "\u01F3": "dz", "\u01C6": "dz", "\u24D4": "e", "\uFF45": "e", "\u00E8": "e", "\u00E9": "e", "\u00EA": "e", "\u1EC1": "e", "\u1EBF": "e", "\u1EC5": "e", "\u1EC3": "e", "\u1EBD": "e", "\u0113": "e", "\u1E15": "e", "\u1E17": "e", "\u0115": "e", "\u0117": "e", "\u00EB": "e", "\u1EBB": "e", "\u011B": "e", "\u0205": "e", "\u0207": "e", "\u1EB9": "e", "\u1EC7": "e", "\u0229": "e", "\u1E1D": "e", "\u0119": "e", "\u1E19": "e", "\u1E1B": "e", "\u0247": "e", "\u025B": "e", "\u01DD": "e", "\u24D5": "f", "\uFF46": "f", "\u1E1F": "f", "\u0192": "f", "\uA77C": "f", "\u24D6": "g", "\uFF47": "g", "\u01F5": "g", "\u011D": "g", "\u1E21": "g", "\u011F": "g", "\u0121": "g", "\u01E7": "g", "\u0123": "g", "\u01E5": "g", "\u0260": "g", "\uA7A1": "g", "\u1D79": "g", "\uA77F": "g", "\u24D7": "h", "\uFF48": "h", "\u0125": "h", "\u1E23": "h", "\u1E27": "h", "\u021F": "h", "\u1E25": "h", "\u1E29": "h", "\u1E2B": "h", "\u1E96": "h", "\u0127": "h", "\u2C68": "h", "\u2C76": "h", "\u0265": "h", "\u0195": "hv", "\u24D8": "i", "\uFF49": "i", "\u00EC": "i", "\u00ED": "i", "\u00EE": "i", "\u0129": "i", "\u012B": "i", "\u012D": "i", "\u00EF": "i", "\u1E2F": "i", "\u1EC9": "i", "\u01D0": "i", "\u0209": "i", "\u020B": "i", "\u1ECB": "i", "\u012F": "i", "\u1E2D": "i", "\u0268": "i", "\u0131": "i", "\u24D9": "j", "\uFF4A": "j", "\u0135": "j", "\u01F0": "j", "\u0249": "j", "\u24DA": "k", "\uFF4B": "k", "\u1E31": "k", "\u01E9": "k", "\u1E33": "k", "\u0137": "k", "\u1E35": "k", "\u0199": "k", "\u2C6A": "k", "\uA741": "k", "\uA743": "k", "\uA745": "k", "\uA7A3": "k", "\u24DB": "l", "\uFF4C": "l", "\u0140": "l", "\u013A": "l", "\u013E": "l", "\u1E37": "l", "\u1E39": "l", "\u013C": "l", "\u1E3D": "l", "\u1E3B": "l", "\u017F": "l", "\u0142": "l", "\u019A": "l", "\u026B": "l", "\u2C61": "l", "\uA749": "l", "\uA781": "l", "\uA747": "l", "\u01C9": "lj", "\u24DC": "m", "\uFF4D": "m", "\u1E3F": "m", "\u1E41": "m", "\u1E43": "m", "\u0271": "m", "\u026F": "m", "\u24DD": "n", "\uFF4E": "n", "\u01F9": "n", "\u0144": "n", "\u00F1": "n", "\u1E45": "n", "\u0148": "n", "\u1E47": "n", "\u0146": "n", "\u1E4B": "n", "\u1E49": "n", "\u019E": "n", "\u0272": "n", "\u0149": "n", "\uA791": "n", "\uA7A5": "n", "\u01CC": "nj", "\u24DE": "o", "\uFF4F": "o", "\u00F2": "o", "\u00F3": "o", "\u00F4": "o", "\u1ED3": "o", "\u1ED1": "o", "\u1ED7": "o", "\u1ED5": "o", "\u00F5": "o", "\u1E4D": "o", "\u022D": "o", "\u1E4F": "o", "\u014D": "o", "\u1E51": "o", "\u1E53": "o", "\u014F": "o", "\u022F": "o", "\u0231": "o", "\u00F6": "o", "\u022B": "o", "\u1ECF": "o", "\u0151": "o", "\u01D2": "o", "\u020D": "o", "\u020F": "o", "\u01A1": "o", "\u1EDD": "o", "\u1EDB": "o", "\u1EE1": "o", "\u1EDF": "o", "\u1EE3": "o", "\u1ECD": "o", "\u1ED9": "o", "\u01EB": "o", "\u01ED": "o", "\u00F8": "o", "\u01FF": "o", "\u0254": "o", "\uA74B": "o", "\uA74D": "o", "\u0275": "o", "\u01A3": "oi", "\u0223": "ou", "\uA74F": "oo", "\u24DF": "p", "\uFF50": "p", "\u1E55": "p", "\u1E57": "p", "\u01A5": "p", "\u1D7D": "p", "\uA751": "p", "\uA753": "p", "\uA755": "p", "\u24E0": "q", "\uFF51": "q", "\u024B": "q", "\uA757": "q", "\uA759": "q", "\u24E1": "r", "\uFF52": "r", "\u0155": "r", "\u1E59": "r", "\u0159": "r", "\u0211": "r", "\u0213": "r", "\u1E5B": "r", "\u1E5D": "r", "\u0157": "r", "\u1E5F": "r", "\u024D": "r", "\u027D": "r", "\uA75B": "r", "\uA7A7": "r", "\uA783": "r", "\u24E2": "s", "\uFF53": "s", "\u00DF": "s", "\u015B": "s", "\u1E65": "s", "\u015D": "s", "\u1E61": "s", "\u0161": "s", "\u1E67": "s", "\u1E63": "s", "\u1E69": "s", "\u0219": "s", "\u015F": "s", "\u023F": "s", "\uA7A9": "s", "\uA785": "s", "\u1E9B": "s", "\u24E3": "t", "\uFF54": "t", "\u1E6B": "t", "\u1E97": "t", "\u0165": "t", "\u1E6D": "t", "\u021B": "t", "\u0163": "t", "\u1E71": "t", "\u1E6F": "t", "\u0167": "t", "\u01AD": "t", "\u0288": "t", "\u2C66": "t", "\uA787": "t", "\uA729": "tz", "\u24E4": "u", "\uFF55": "u", "\u00F9": "u", "\u00FA": "u", "\u00FB": "u", "\u0169": "u", "\u1E79": "u", "\u016B": "u", "\u1E7B": "u", "\u016D": "u", "\u00FC": "u", "\u01DC": "u", "\u01D8": "u", "\u01D6": "u", "\u01DA": "u", "\u1EE7": "u", "\u016F": "u", "\u0171": "u", "\u01D4": "u", "\u0215": "u", "\u0217": "u", "\u01B0": "u", "\u1EEB": "u", "\u1EE9": "u", "\u1EEF": "u", "\u1EED": "u", "\u1EF1": "u", "\u1EE5": "u", "\u1E73": "u", "\u0173": "u", "\u1E77": "u", "\u1E75": "u", "\u0289": "u", "\u24E5": "v", "\uFF56": "v", "\u1E7D": "v", "\u1E7F": "v", "\u028B": "v", "\uA75F": "v", "\u028C": "v", "\uA761": "vy", "\u24E6": "w", "\uFF57": "w", "\u1E81": "w", "\u1E83": "w", "\u0175": "w", "\u1E87": "w", "\u1E85": "w", "\u1E98": "w", "\u1E89": "w", "\u2C73": "w", "\u24E7": "x", "\uFF58": "x", "\u1E8B": "x", "\u1E8D": "x", "\u24E8": "y", "\uFF59": "y", "\u1EF3": "y", "\u00FD": "y", "\u0177": "y", "\u1EF9": "y", "\u0233": "y", "\u1E8F": "y", "\u00FF": "y", "\u1EF7": "y", "\u1E99": "y", "\u1EF5": "y", "\u01B4": "y", "\u024F": "y", "\u1EFF": "y", "\u24E9": "z", "\uFF5A": "z", "\u017A": "z", "\u1E91": "z", "\u017C": "z", "\u017E": "z", "\u1E93": "z", "\u1E95": "z", "\u01B6": "z", "\u0225": "z", "\u0240": "z", "\u2C6C": "z", "\uA763": "z" };

    function defaultEscapeMarkup(markup) {
        var replaceMap = {
            '\\': '&#92;',
            '&': '&amp;',
            '<': '&lt;',
            '>': '&gt;',
            '"': '&quot;',
            "'": '&#39;',
            "/": '&#47;'
        };

        return String(markup).replace(/[&<>"'\/\\]/g, function (match) {
            return replaceMap[match];
        });
    }

    function stripDiacritics(str) {
        var ret, i, l, c;

        if (!str || str.length < 1) return str;

        ret = "";
        for (i = 0, l = str.length; i < l; i++) {
            c = str.charAt(i);
            ret += diacritics[c] || c;
        }
        return ret;
    }

    var apply = function (select) {

        var removeInHightlites = function (hightlites) {
            for (var n = hightlites.length - 1; n >= 0; n--) {
                var h1 = hightlites[n];
                for (var j = n - 1; j >= 0; j--) {
                    var h2 = hightlites[j];
                    if (h1.start >= h2.start && h1.end <= h2.end) {
                        hightlites.splice(n, 1);
                        break;
                    }
                }
            }
        };

        var expandHightlites = function (hightlites) {
            for (var n = hightlites.length - 1; n >= 0; n--) {
                var h1 = hightlites[n];
                for (var j = n - 1; j >= 0; j--) {
                    var h2 = hightlites[j];

                    var ss = (h2.start <= h1.start) && (h1.start <= h2.end);
                    var se = (h2.start <= h1.end) && (h1.end <= h2.end);
                    if (ss) {
                        h2.end = h1.end;
                        h2.length = h2.end - h2.start + 1;
                        hightlites.splice(n, 1);
                    } else if (se) {
                        h2.start = h1.start;
                        h2.length = h2.end - h2.start + 1;
                        hightlites.splice(n, 1);
                    }
                }
            }
        };

        function markMatch(text, terms, markup) {
            if (text == undefined || text == "null") {
                markup.push("");
                return;
            }
            var j;
            var textToSearch = stripDiacritics(text.toUpperCase());
            var hightlites = [];
            for (j = 0; j < terms.length; j++) {
                var pStart = 0;
                var p;
                var term = stripDiacritics(terms[j].toUpperCase());
                if (!term)
                    continue;

                do {
                    p = textToSearch.indexOf(term, pStart);
                    if (p > -1) {
                        pStart = p + term.length;

                        hightlites.push({
                            start: p,
                            length: term.length,
                            end: pStart
                        });

                        removeInHightlites(hightlites);
                        expandHightlites(hightlites);
                    }
                } while (p > -1);
            }

            var result = text;
            var diff = 0;
            hightlites.sort(function (a, b) { return a.start - b.start; });
            for (j = 0; j < hightlites.length; j++) {
                var h = hightlites[j];
                var textToReplace = result.substring(h.start + diff, h.end + diff);
                var replaceHtml = '<span class="select2-match">' + textToReplace + "</span>";

                result = result.substring(0, h.start + diff) + replaceHtml + result.substring(h.end + diff, result.length);

                diff += (replaceHtml.length - textToReplace.length);
            }

            markup.push(result);
        }

        //#endregion

        var url = select.data("action");
        var placeholder = select.data("placeholder");
        var initAction = select.data("init-values") || select.data("init-value");
        var isMultiple = select.hasClass("multi-select");
        var useHierarchy = !!select.data("use-hierarchy");
        var table = select.data("table");
        var tableDisplay = select.data("table-display");
        var searchFields = select.data("search-fields");
        var columns = [];
        var columnsDisplay = [];
        var searchFieldsDefinition = [];
        var isReadOnly = select.is("[disabled]") || select.is("[readonly]");
        var showPresencePopup = select.data("show-presence-popup");
        var useLync = select.data("use-lync");

        if (isReadOnly && (!select.val() || select.val() == select.data("default-value"))) {
            select.data("original-placeholder", placeholder);
            placeholder = "";
            select.data("placeholder", "");
            select.attr("data-placeholder", "");
        }

        var options = {
            allowClear: select.hasClass("select-clear"),
            placeholder: placeholder,
            multiple: isMultiple,
            minimumInputLength: 0,
            dropdownAutoWidth: true,
            ajax: {
                // instead of writing the function to execute the request we use Select2's convenient helper
                url: url,
                quietMillis: 300,
                data: function (term, page) {
                    var ajaxData = select.data("ajax-data");
                    if (typeof ajaxData == "string") {
                        ajaxData = $.parseJSON(ajaxData);
                    }
                    var data = $.extend({}, {
                        term: term, // search term
                        page: page
                    }, ajaxData);

                    var selectData = $(select).data();
                    $.each(selectData, function (key, value) {
                        if (key.indexOf("ajaxData") == 0) {
                            var selectField = key.substring("ajaxData".length);
                            data[selectField] = value;
                        }
                    });

                    return data;
                },
                results: function (data, page) { // parse the results into the format expected by Select2.
                    // since we are using custom formatting functions we do not need to alter remote JSON data

                    var items = data;
                    var more = false; // whether or not there are more results available
                    if (data.total) {
                        items = data.items;
                        more = page * (data.pageSize || 20) < data.total;
                    } else {
                        if (data.pageSize) {
                            items = [];
                        }
                    }

                    if (useHierarchy === true) {
                        var selectedIds = select.select2('val');
                        var selectableGroups = $.map(items, function (group) {
                            var areChildrenAllSelected = true;
                            $.each(group.children, function (i, child) {
                                if ($.inArray(child.id, selectedIds) < 0) {
                                    areChildrenAllSelected = false;
                                    return false;
                                }
                                return true;
                            });
                            return !areChildrenAllSelected ? group : null;
                        });
                        return { results: selectableGroups };
                    }

                    // notice we return the value of more so Select2 knows if more results can be loaded
                    return { results: items, more: more };
                }
            },
            initSelection: function (element, callback) {

                if (initAction) {

                    var value = select.val();
                    var defaultValue = select.data("default-value");
                    if (defaultValue != value) {
                        var indicator = window.ajaxIndicator.show(select.select2("container"));
                        $.ajax({
                            url: initAction,
                            data: {
                                value: value
                            },
                            complete: function () {
                                if (indicator)
                                    indicator.remove();
                            },
                            success: function (data) {

                                var items = data;
                                if (data.total && data.items) {
                                    items = data.items;
                                }

                                if (items && !data.error) {
                                    var dataItems = [];
                                    $.each(items, function (index, el) {
                                        if (!el.error) {
                                            if (!isMultiple) {
                                                callback(el);
                                            } else {
                                                dataItems.push(el);
                                            }
                                        }
                                    });
                                    if (isMultiple && dataItems.length) {
                                        callback(items);
                                    }
                                }
                                select.triggerHandler("select2-custom-loaded-selection");
                            }
                        });
                    }

                } else {
                    var text = select.data("text");
                    if (select.val() && text) {
                        var dataObj = { id: select.val(), text: select.data("text") };
                        if (table) {
                            var tableObj = {};

                            for (var i = 0; i < columns.length; i++) {
                                tableObj[columns[i].name] = select.data(columns[i].name.toLowerCase());
                            }
                            dataObj = $.extend(dataObj, tableObj);
                        }
                        callback(dataObj);
                        select.trigger("select2-custom-loaded-selection");
                    }
                }
            }
        };

        var isFieldSearchEnabled = function (fieldName) {
            for (var j = 0 ; j < searchFieldsDefinition.length ; j++)
                if (fieldName == searchFieldsDefinition[j])
                    return true;

            return false;
        };

        var formatSelectionCustom = function (data, element, escapeMarkup) {
            if (!data)
                return undefined;

            var a = element.parents(".select2-choice,.select2-search-choice");
            if (table && tableDisplay) {
                //different format - display hint to extend info for selected items

                a.tipTip({
                    attribute: "data-hint",
                    getContent: function () {
                        var result = "";

                        var selectData = select.select2("data");
                        if (selectData) {
                            for (var j = 0; j < columns.length; j++) {
                                var value = selectData[columns[j].name];
                                if (value) {
                                    if (result) {
                                        result += " - ";
                                    }
                                    result += selectData[columns[j].name];
                                }
                            }
                        }
                        return result;
                    }
                });
            }

            var text = !useHierarchy ? escapeMarkup(data.text) : escapeMarkup(data.group + " - " + data.text);

            if (useLync && data.email) {
                var o = {
                    email: data.email,
                    showPresencePopup: showPresencePopup
                };
                var elt = !isMultiple ? element : element.closest(".hint-processed");
                elt.data("email", data.email); //save options in the element data attributes to allow refreshing
                elt.data("show-presence-popup", showPresencePopup);
                var lync = new Lync();
                lync.registerNewPrincipal(elt, o);
            }

            var linkToFormat = select.data("selected-format-as-link");
            var formatAsLink = isReadOnly || select.data("selected-format-as-link-always");

            if (formatAsLink && linkToFormat && data.id) {
                linkToFormat = linkToFormat.replace("[id]", data.id);

                a.addClass("select2-link");
                a.off();
                a.find(".select2-search-choice-close").on("click", function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                    select.select2("data", null);
                    select.trigger("select2-removed");
                });

                a.attr("href", linkToFormat);
                a.attr("onclick", "");
                a.addClass("ajax-modal-window");
                a.click(function () {
                    site.processModalAnchorClick(this);
                });

                a.find(".select2-arrow").hide();
            }
            return text;
        };

        if (!table) {
            options.formatSelection = function (data, element, escapeMarkup) {
                return formatSelectionCustom(data, element, escapeMarkup);
            };
        }
        if (table) {

            if (searchFields) {
                searchFieldsDefinition = searchFields.split(',');
            }

            var readColumns = function (columnsAsString) {
                var columnsDefinitionArray = columnsAsString.split('|');

                var columnDefinitions = [];
                for (var j = 0; j < columnsDefinitionArray.length; j++) {
                    var columnDefinitionsAsString = columnsDefinitionArray[j].split(':');
                    columnDefinitions.push({
                        name: columnDefinitionsAsString[0],
                        width: parseInt(columnDefinitionsAsString[1], 10),
                        displayName: columnDefinitionsAsString[2]
                    });
                }
                
                var sum = function (array, selector) {
                    var result = 0;
                    for (var i = 0; i < array.length; i++) {
                        result += selector(array[i]);
                    }
                    return result;
                };

                var sumWidth = sum(columnDefinitions, function (item) {
                    return item.width;
                });

                var result = $.map(columnDefinitions, function (item) {
                    return {
                        name: item.name,
                        width: Math.round(item.width * (100 / sumWidth)) + "%",
                        displayName: item.displayName
                    }
                });

                return result;
            };

            columns = readColumns(table);
            if (tableDisplay) {
                columnsDisplay = readColumns(tableDisplay);
            } else {
                columnsDisplay = columns;
            }
            if (!searchFieldsDefinition.length) {
                for (var i = 0; i < columns.length; i++) {
                    searchFieldsDefinition.push(columns[i].name);
                }
            }

            var formatResult = function (item, context) {
                var j, markup = "";

                var formatValue = function (val, t) {
                    var matchValue = [];
                    markMatch(val, t, matchValue);
                    val = matchValue.join("");
                    return val;
                };

                for (j = 0; j < columns.length; j++) {
                    var value = defaultEscapeMarkup(item[columns[j].name]);
                    if (context && context.term && isFieldSearchEnabled(columns[j].name)) {
                        var term = context.term;

                        if (select.data("search-mode") == "split-space") {
                            var terms = term.split(' ');
                            value = formatValue(value, terms);
                        } else {
                            value = formatValue(value, [term]);
                        }
                    }
                    markup += "<td class='table-column' width='" + columns[j].width + "'>" + value + "</td>";
                }
                return markup;
            };
            options.formatSelectionCssClass = function (data) {
                if (data && data.isHighlited) {
                    return "select2-result-hightlited";
                }
                return "";
            };
            options.formatSelection = function (item, element, escapeMarkup) {
                element.removeClass("select2-result-hightlited");
                var result = "";
                for (var j = 0; j < columnsDisplay.length; j++) {
                    var value = item[columnsDisplay[j].name];
                    if (value) {
                        if (result) {
                            result += " - ";
                        }
                        result += item[columnsDisplay[j].name];
                    }
                }
                if (!result)
                    result = "[Invalid selection]";

                item.text = result;

                return formatSelectionCustom(item, element, escapeMarkup);
            };
            if (table) {
                options.populateResults = function (container, results, context) {
                    var i, l, result, selectable, disabled, formatted;

                    var createNode = function (item, isDisabled) {
                        var node = $("<tr></tr>");
                        node.addClass("select2-results-dept-0");
                        node.addClass("select2-result");
                        node.addClass("select2-result-label");
                        node.addClass(selectable ? "select2-result-selectable" : "select2-result-unselectable");
                        if (item && item.isHighlited) {
                            node.addClass("select2-result-hightlited");
                        }
                        if (isDisabled) {
                            node.addClass("select2-disabled");
                        }

                        formatted = formatResult(item, context);

                        node.append(formatted);
                        return node;
                    };

                    var tbl = '<li><table class="table-result">';

                    if (results.length) {
                        tbl += '<thead><tr class="table-caption">';
                        for (var j = 0; j < columns.length; j++) {
                            var name = columns[j].displayName;
                            if (!name)
                                name = columns[j].name;

                            var headerClass = "table-column";
                            if (searchFields && isFieldSearchEnabled(columns[j].name)) {
                                headerClass += " dropdown-column-filter";
                            }
                            tbl += '<th class="' + headerClass + '" width="' + columns[j].width + '">' + name + '</th>';
                        }
                        tbl += "</tr><thead>";
                    }
                    tbl += "<tbody></tbody></table></li>";
                    tbl = $(tbl).find("table");
                    var tbody = $(tbl).find("tbody");

                    container.append(tbl);
                    for (i = 0, l = results.length; i < l; i = i + 1) {
                        result = results[i];

                        disabled = (result.disabled === true);
                        selectable = (!disabled);

                        var itemNode = createNode(result, disabled);
                        itemNode.data("select2-data", result);
                        tbody.append(itemNode);
                    }
                };
            }
        }
        var removeTableHeader = function () {
            var select2 = select.data("select2");
            if (!select2)
                return;

            var dropdown = select2.dropdown;
            $(".table-head", dropdown).remove();
        };
        select.on("select2-open", function () {
            if (table) {
                removeTableHeader();
            }
        });
        if (table) {
            select.addClass("select-table");
            options.formatNoMatches = function () {
                removeTableHeader();
                return "No matches found";
            };
        }

        select.select2(options);

        if (useHierarchy) {
            select.on('select2-selecting', function (e) {
                var $select = $(this);
                var data = e.object;
                if (data && data.isNode) {
                    $select.select2('data', $select.select2('data').concat(data.children));
                    $select.select2('close');
                }
            });
        }

        select.on("select2-loaded", function () {
            if (table) {
                var dropdown = select.data("select2").dropdown;
                var results = dropdown.find(".select2-results");
                var htmlTable = results.find(".table-result");
                var columnWidths = [];
                $("th", htmlTable).each(function () {
                    columnWidths.push($(this).width());
                });
                htmlTable.addClass("table-fixed");
                $("tr", htmlTable).each(function () {
                    $("td,th", $(this)).each(function (index) {
                        $(this).attr("width", columnWidths[index] + "px");
                    });
                });

                $(".table-head", dropdown).remove();
                var resultsCount = $("tr", htmlTable).length;

                var headTable = dropdown.find(".table-head");
                if (resultsCount) {
                    headTable.show();
                    if (!headTable.length) {
                        headTable = $('<table class="table-result table-head table-fixed"></table>');
                        headTable.insertBefore(results);
                        var header = $("thead", results);
                        headTable.append(header);
                    }
                    $("th", headTable).each(function (index) {
                        $(this).attr("width", columnWidths[index] + "px");
                    });
                    var tableWidth = htmlTable.width();
                    //table with fixed layout has max width is automatically big, so update both tables
                    headTable.width(tableWidth + "px");
                    htmlTable.width(tableWidth + "px");
                } else {
                    headTable.hide();
                }
            }
        });
        select.on("select2-selected", function (e) {
            var action = $(this).data("on-select");
            if (action) {
                site.callFunction(action, e);
            }
            if (select.parents("form").length) {
                $(this).valid();
                $(this).focus();
            }
        });

        if (isReadOnly) {
            select.select2("readonly", true);
        }
    };

    this.apply = apply;
}